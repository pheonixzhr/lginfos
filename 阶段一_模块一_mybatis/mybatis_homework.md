#1.Mybatis动态sql是做什么的？都有哪些动态sql？简述一下动态sql的执行原理.

##解答：执行业务较为复杂的sql时可以使用动态SQL，可以根据传入的参数拼接不同的sql语句，从而应对多条件多环境下的开发需求。比如<if>，<where>,<choose>,<foreach>等。

##动态SQL的执行和普通sql的执行的区别在于，在对mapper配置文件的读取时，获取到动态sql标签会根据传入的参数判断该对应情况下需要拼接的sql，获取#{}传来的参数，进而拼成完整的sql去执行。

#2.Mybatis是否支持延迟加载？如果支持，它的实现原理是什么？

##解答：支持延迟加载，即关联查询时，并不是任何时候都需要立即获取关联查询的结果，当需要获取时，再通过执行查询获取关联查询结果。Mybatis配置文件中通过两个属性lazyLoadingEnabled和aggressiveLazyLoading来控制延迟加载和按需加载，默认具为false。通过在resultMap标签内设置collection标签，在collection标签下设置select属性，将延迟查询的sql的id进行确定。

##当主查询执行时，可以获取查询结果，若需要关联查询，那么会再次执主查询对应的resultMap下的connection中的select标签下的查询操作，获取关联信息。

#3.Mybatis都有哪些Executor执行器？它们之间的区别是什么？

##解答：这个接口有三个常用的实现类：BatchExecutor--重用语句并执行批量操作,ReuseExecutor--重用预处理语句prepared statement,SimpleExecutor--普通的执行器，默认使用。

#4.简述下Mybatis的一级、二级缓存（分别从存储结构、范围、失效场景。三个方面来作答）？

##1、解答：一级缓存：范围：sqlsession级别的缓存，线程不安全；存储结构：基于Perpetualcache的hashmap结构；失效：执行commit操作的时候或者sqlsession执行slearCache和close操作的时候会清空缓存；

##二级缓存：范围：namespace级别，可以跨sqlsession；存储结构：同上；失效：当sqlsession执行commit/close 时，会清空一级缓存并将缓存内容缓存到二级缓存；当该作用域执行c/u/d操作时，会更新缓存。

#5.简述Mybatis的插件运行原理，以及如何编写一个插件？

##解答：①创建Myplugin，并实现Interceptor接口；

​				②在该类上添加@intercepts（{

​											@Signature（type = “”,

​												Method = “”,

​													Args = “”）

​												}）注解，添加相应内容，从而判断该插件需要执行的范围；

​				③重载实现的接口的方法如：intercept()，plugin()，setPropertise();

​				④主配置文件 需要配置plugins标签，并确定plugin标签内的interceptor属性的全路径和配					置参数。